/**
 * 
 */
package com.flowable.springboot.example.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.flowable.springboot.example.entity.Person;

/**
 * @author Avinash
 *
 */
@Repository
public interface PersonRepository extends JpaRepository<Person, Long> {

    Person findByUsername(String username);
}