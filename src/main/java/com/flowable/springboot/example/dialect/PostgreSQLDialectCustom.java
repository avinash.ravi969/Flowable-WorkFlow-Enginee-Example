/**
 * 
 */
package com.flowable.springboot.example.dialect;

import java.sql.Types;

import org.hibernate.dialect.PostgreSQL94Dialect;

/**
 * @author Avinash
 *
 */
public class PostgreSQLDialectCustom extends PostgreSQL94Dialect {

	public PostgreSQLDialectCustom() {
		super();
		registerColumnType(Types.ARRAY, "int8[$1]");
		registerColumnType(Types.ARRAY, "text[]");
		registerColumnType(Types.JAVA_OBJECT, "json");
	}
}
