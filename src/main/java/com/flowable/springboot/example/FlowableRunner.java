/**
 * 
 */
package com.flowable.springboot.example;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.flowable.springboot.example.service.FlowableTaskService;

/**
 * @author Avinash
 *
 */
@SpringBootApplication
public class FlowableRunner {
	
	 public static void main(String[] args) {
	        SpringApplication.run(FlowableRunner.class, args);
	    }
	 
	 @Bean
	 public CommandLineRunner init(final FlowableTaskService flowableTaskService) {

	     return new CommandLineRunner() {
	     	public void run(String... strings) throws Exception {
	     		flowableTaskService.createDemoUsers();
	         }
	     };
	 }
}
